#Peticlette - Sources

Ce repo contient les sources de l'application **Petitclette**

(Application disponible sur ce [repo](https://bitbucket.org/colinpeyrat/ddi_peticlette))

##Installation
```
$ npm install
$ bower install
```

## Utilisation

- `gulp serve` pour lancer le serveur de développement
- `gulp` pour compiler les sources et copier le contendu de `dist/` dans `www/` du projet Cordova (à cause de Browserify, le code JS minifié peut parfois contenir des erreurs, dans ce cas il suffit de refaire un `gulp build` et de recopier `scripts/` dans `www/` du projet Cordova)