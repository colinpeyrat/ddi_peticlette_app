var LoginView = require('../views/login-view');
var SignInView = require('../views/sign-in-view');
var PathView = require('../views/path-view');
var PathDetailView = require('../views/path-detail-view');
var HistoricalView = require('../views/historical-view');
var ProfileView = require('../views/profile-view');
var AddChildrenView = require('../views/add-children-view');
var SettingsView = require('../views/settings-view');

var EventBus = require('../helpers/event-bus');

var router = Backbone.Router.extend({

    routes: {
        'historical': 'historical',
        'profile': 'profile',
        'addChildren': 'addChildren',
        'settings': 'settings',
        'path/:id': 'getPath',
        'login': 'login',
        'signIn': 'signIn',
        '*path': 'path' // The default route
    },

    initialize: function (param) {
        console.log('routers/router: initialize');

        this.app = param.app;
        this.listenTo(this, 'route', this.onRouteChange);
    },

    historical: function () {
        console.log('routers/router: historical');

        if (!this.app.user) {
            this.navigate('login', {trigger: true});
        } else {
            this.app.currentView = new HistoricalView({
                model: this.app.paths,
                user: this.app.user
            });
        }
    },

    signIn: function () {
        console.log('routers/router: signIn');

        this.app.currentView = new SignInView({
            model: this.app.users
        });
        this.app.menuView.disable();
        this.app.menuView.render();
    },

    login: function () {
        console.log('routers/router: login');

        if (this.app.user) {
            this.navigate('login', {trigger: true});
        } else {
            this.app.currentView = new LoginView({
                model: this.app.users
            });

            this.app.menuView.disable();
            this.app.menuView.render();
        }
    },

    path: function () {
        console.log('routers/router: path');
        this.app.currentView = new PathView({
            model: this.app.paths
        });
        this.app.menuView.enable();
        this.app.menuView.render();
    },

    getPath: function (id) {
        console.log('routers/router: getPath', id);

        if (!this.app.user) {
            this.navigate('login', {trigger: true});
        } else {
            this.app.currentView = new PathDetailView({
                model: this.app.paths.get(id),
                user: this.app.user
            });
            EventBus.trigger('menu:change', 'path');
        }
    },

    profile: function () {
        console.log('routers/router: profile');

        if (!this.app.user) {
            this.navigate('login', {trigger: true});
        } else {
            this.app.currentView = new ProfileView({
                model: this.app.user
            });
        }
    },

    addChildren: function () {
        console.log('routers/router: addChildren');
        this.app.currentView = new AddChildrenView({model: this.app.user});
    },

    settings: function () {
        console.log('routers/router: settings');
        if (!this.app.user) {
            this.navigate('login', {trigger: true});
        } else {
            this.app.currentView = new SettingsView();
        }
    },

    renderCurrent: function () {
        console.log('routers/router: renderCurrent');

        this.app.render();
    },

    current: function () {
        var Router = this,
            fragment = Backbone.history.fragment,
            routes = _.pairs(Router.routes),
            route = null, params = null, matched;

        matched = _.find(routes, function (handler) {
            route = _.isRegExp(handler[0]) ? handler[0] : Router._routeToRegExp(handler[0]);
            return route.test(fragment);
        });

        if (matched) {
            // NEW: Extracts the params using the internal
            // function _extractParameters
            params = Router._extractParameters(route, fragment);
            route = matched[1];
        }

        return {
            route: route,
            fragment: fragment,
            params: params
        };
    },

    execute: function (callback, args, name) {
        console.log(this.current(), this.app.currentView);
        this.app.previousView = this.app.currentView || {
                leave: function (callback) {
                    console.log('Default leave');
                    callback();
                }, remove: function () {
                }
            };

        if (callback) callback.apply(this, args); //this must be called to pass to next route
    },

    onRouteChange: function () {
        console.log('routers/router: onRouteChange');

        this.app.previousView.leave = this.app.previousView.leave || function (callback) {
                callback();
            };

        if (this.app.currentView != this.app.previousView || Backbone.history.fragment == '') {
            this.app.previousView.leave(function () {
                this.app.previousView.remove();
                this.renderCurrent();
                if (this.app.currentView.enter) {
                    this.app.currentView.enter();
                }
            }.bind(this));
        }
    }

});

module.exports = router;