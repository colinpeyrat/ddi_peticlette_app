var Path = Backbone.Model.extend({
    idAttribute: '_id',

    defaults: {
        _id: null,
        date: null,
        start: {
            adress: null,
            pos: null
        },
        end: {
            adress: null,
            pos: null
        },
        companion: null,
        bookings: [],
        bookingsLimit: 10
    },

    getBooking: function (userId) {
        return _.find(this.get('bookings'), function (booking) {
            return booking.parent == userId;
        });
    },

    addBooking: function (userId, booking) {
        var userBooking = this.getBooking(userId);

        var _bookings = _.clone(this.get('bookings'));

        // if user already book this path
        if (userBooking) {
            var indexUserBooking = _.findIndex(this.get('bookings'), function (booking) {
                return booking.parent == userId;
            });

            // update the booking of user
            _bookings[indexUserBooking] = booking;

        } else {
            // add the booking of user to booking
            _bookings.push(booking);
        }

        this.set('bookings', _bookings);

        this.save();
    }
});

module.exports = Path;
