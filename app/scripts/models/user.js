var Server = require('../helpers/server');
var EventBus = require('../helpers/event-bus');

var Path = Backbone.Model.extend({
    idAttribute: '_id',

    url: function () {
        return Server.url + '/users/' + this.id;
    },

    defaults: {
        _id: null,
        firstname: null,
        lastname: null,
        login: null,
        password: null,
        childrens: []
    },

    deleteChildren: function (id) {

        var _childrens = _.filter(_.clone(this.get('childrens')), function (children) {
            return id.toString() !== children._id;
        });

        EventBus.trigger('children:delete', id);

        this.set('childrens', _childrens);

        this.save();
    },

    addChildren: function (firstname, lastname, birthdate) {
        var _childrens = _.clone(this.get('childrens'));

        _childrens.push({
            _id: _.uniqueId(),
            firstname: firstname,
            lastname: lastname,
            birthdate: birthdate
        });

        this.set('childrens', _childrens);

        this.save();
    }
});

module.exports = Path;
