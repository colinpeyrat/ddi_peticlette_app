var Path = require('../models/path');

var Server = require('../helpers/server');
var EventBus = require('../helpers/event-bus');


var Paths = Backbone.Collection.extend({
    model: Path,

    url: function () {
        return Server.url + '/paths';
    },

    comparator: function (property) {
        return selectedStrategy.apply(this.get(property));
    },

    initialize: function () {
        this.changeSort('date');

        // I listen on delete of a children to create a "cascade delete"
        this.listenTo(EventBus, 'children:delete', this.onChildrenDeleted);
    },

    strategies: {
        date: function (path) {
            return path.get('date');
        }
    },

    changeSort: function (sortProperty) {
        this.comparator = this.strategies[sortProperty];
    },

    getByUser: function (userId) {
        var pathsOfUser = [];
        this.each(function (path) {
            _.each(path.get('bookings'), function (booking) {
                if (booking.parent === userId.toString() && booking.childrens.length) {
                    pathsOfUser.push(path);
                }
            });
        });

        return pathsOfUser;
    },

    onChildrenDeleted: function (id) {
        var _bookings;
        this.each(function (path) {
            _bookings = _.filter(path.get('bookings'), function (booking) {
                booking.childrens = _.without(booking.childrens, _.findWhere(booking.childrens, {
                    _id: id
                }));
            });
            path.set('bookings', _bookings);
            path.save();
        });
    }
});


module.exports = Paths;
