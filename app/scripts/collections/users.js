var User = require('../models/user');

var Server = require('../helpers/server');

var Users = Backbone.Collection.extend({
    model: User,
    url: function () {
        return Server.url + '/users';
    },

    initialize: function () {
    },

    /**
     * Verify is user already exist if this login
     * @param login
     */
    getByLogin: function (login) {

        var alreadyExist = false;

        this.each(function (user) {
            if (user.get('login') === login) {
                alreadyExist = true;
            }
        });

        return alreadyExist;
    }
});


module.exports = Users;
