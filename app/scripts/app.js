var Paths = require('./collections/paths');
var Users = require('./collections/users');
var menuView = require('./views/menu-view');

var router = require('./routers/router');

var EventBus = require('./helpers/event-bus');

var App = Backbone.View.extend({

    el: '#app',
    $content: null,
    currentView: null,

    initialize: function () {

        this.$content = $('#content');

        // Collections and modlels
        // -----------------------

        this.paths = new Paths();

        this.users = new Users();


        // Listeners
        // ---------

        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
        this.listenTo(this.users, 'reset', this.onUserLoaded);
        this.listenTo(EventBus, 'user:logged', this.onUserLogged);
        this.listenTo(EventBus, 'user:logout', this.onUserLogout);
        this.listenTo(EventBus, 'user:signIn', this.onUserSignIn);


        // Persistent view
        // ---------------

        this.menuView = new menuView();
        this.menuView.disable();


        // Router
        // ------

        this.router = new router({
            app: this
        });
    },

    onDeviceReady: function () {
        this.users.fetch({reset: true});
    },

    onUserLoaded: function () {
        TweenMax.delayedCall(2.75, function () {
            this.$el.removeClass('is-loading');
            Backbone.history.start();
        }, {}, this)
    },

    onUserLogged: function (user) {
        this.user = user;
    },

    onUserSignIn: function (datas) {
        this.users.create(datas);
    },

    onUserLogout: function () {
        this.user = false;
        Backbone.history.navigate('login', true);
    },

    render: function () {
        if (this.currentView) {
            this.$content.html(this.currentView.render().el);
        }
    }
});

var app = new App();


console.log(app);