var EventBus = require('../helpers/event-bus');

var AddChildrenView = Backbone.View.extend({

    tagName: 'div',
    className: 'content-inner',
    id: 'contentInner',

    hasError: false,

    $firstname: null,
    $lastname: null,
    $birthdate: null,
    $btn: null,

    template: _.template(require('../templates/add-children.html')),

    events: {
        'click #add': 'addChildren'
    },

    initialize: function () {
        console.log('view/path-view: initialize');

        this.render();
    },

    addChildren: function (e) {
        e.preventDefault();
        e.stopPropagation();

        _.each([this.$login, this.$password, this.$birthdate], function (value) {
            if (!value.val().length) {
                value.addClass('has-error');
                this.hasError = true;
            } else {
                value.removeClass('has-error');
                this.hasError = false;
            }
        }, this);


        if (this.isValidDate(this.$birthdate.val())) {
            this.$birthdate.removeClass('has-error');
            var birthdate = new Date(this.formatDate(this.$birthdate.val()));
            this.hasError = false;
        } else {
            this.$birthdate.addClass('has-error');
            this.hasError = true;
        }

        if (!this.hasError) {
            var firstname = this.$login.val();
            var lastname = this.$password.val();

            this.model.addChildren(firstname, lastname, birthdate);

            EventBus.trigger('menu:change', 'profile');
            Backbone.history.navigate('profile', true);
        }
    },

    isValidDate: function (s) {
        var bits = s.split('/');
        var d = new Date(bits[2], bits[1] - 1, bits[0]);
        return d && (d.getMonth() + 1) == bits[1];
    },

    formatDate: function (date) {
        date = date.split('/');
        return date[1] + '/' + date[0] + '/' + date[2];
    },

    enter: function () {
        console.log('view/path-view: enter');
        TweenMax.from(this.el, 0.25, {autoAlpha: 0, y: 200, ease: Sine.easeOut, force3D: true});
    },

    leave: function (callback) {
        console.log('view/path-view: leave');
        TweenMax.to(this.el, 0.15, {
            y: 200,
            autoAlpha: 0,
            ease: Sine.easeOut,
            force3D: true,
            onComplete: function () {
                callback();
            }
        });
    },

    render: function () {
        this.$el.html(this.template());

        this.$login = this.$('#firstname');
        this.$password = this.$('#lastname');
        this.$birthdate = this.$('#birthdate');
        this.$btn = this.$('#add');

        return this;
    }
});

module.exports = AddChildrenView;