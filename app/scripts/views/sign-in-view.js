var EventBus = require('../helpers/event-bus');

var LoginView = Backbone.View.extend({

    tagName: 'div',
    className: 'content-inner',
    id: 'contentInner',

    hasError: false,

    $firstname: null,
    $lastname: null,
    $login: null,
    $password: null,
    $passwordConfirm: null,
    $messageWrapper: null,

    template: _.template(require('../templates/sign-in.html')),

    events: {
        'click #signInBtn': 'signIn'
    },

    initialize: function () {
        console.log('view/signin-view: initialize');
        this.model.fetch();


        // wait to load all users form server before display form
        this.listenTo(this.model, 'sync', this.render);
    },

    enter: function () {
        console.log('view/signin-view: enter');
        TweenMax.from(this.el, 0.25, {autoAlpha: 0, x: 200, ease: Sine.easeOut, force3D: true});
    },

    leave: function (callback) {
        console.log('view/signin-view: leave');
        TweenMax.to(this.el, 0.15, {
            autoAlpha: 0,
            x: 200,
            ease: Sine.easeOut,
            force3D: true,
            onComplete: function () {
                callback();
            }
        });
    },

    signIn: function (e) {
        e.preventDefault();
        e.stopPropagation();

        console.log('sign in');

        var inputs = [this.$firstname, this.$lastname, this.$login, this.$password, this.$passwordConfirm];

        _.each(inputs, function (value) {
            if (!value.val().length) {
                value.addClass('has-error');
                this.hasError = true;
            } else {
                value.removeClass('has-error');
                this.hasError = false;
            }
        }, this);


        var firstname = this.$firstname.val();
        var lastname = this.$lastname.val();
        var pseudo = this.$login.val();
        var password = this.$password.val();
        var passwordConfirm = this.$passwordConfirm.val();

        if (password !== passwordConfirm) {
            this.$password.addClass('has-error');
            this.$passwordConfirm.addClass('has-error');
            this.hasError = true;
            this.$messageWrapper.html('Les mots de passes saisies ne sont pas identiques');
        } else {
            this.$password.removeClass('has-error');
            this.$passwordConfirm.removeClass('has-error');
            this.hasError = false;
            this.$messageWrapper.html('');
        }

        if (this.model.getByLogin(pseudo)) {
            this.hasError = true;
            this.$messageWrapper.html('Ce pseudo existe déjà.');
        } else {
            this.hasError = false;
        }

        if (!this.hasError) {

            this.$messageWrapper.html('');


            var datas = {
                _id: _.uniqueId(),
                firstname: firstname,
                lastname: lastname,
                login: pseudo,
                password: password
            };
            EventBus.trigger('user:signIn', datas);
            Backbone.history.navigate('login', true);
        }
    },

    render: function () {
        this.$el.html(this.template());

        this.$firstname = this.$('#firstname');
        this.$lastname = this.$('#lastname');
        this.$login = this.$('#login');
        this.$password = this.$('#password');
        this.$passwordConfirm = this.$('#passwordConfirm');
        this.$messageWrapper = this.$('#messageWrapper');

        return this;
    }
});

module.exports = LoginView;