var PathListItem = Backbone.View.extend({
    tagName: 'article',
    className: 'path-list-item',

    template: _.template(require('../../templates/historical/list-item.html')),

    // Dans une classe backbone par convention initialize est la premiere méthode et render la derniere
    initialize: function () {
        this.render();
        // On rend la vue à nouveau quand le model d'un listItem change
        this.listenTo(this.model, 'change', this.render);
    },

    render: function () {
        // On passe les data pour peupler le template
        // et on le passe en html du el via jQuery
        this.$el.html(this.template(this.model.toJSON()));
    }

});

module.exports = PathListItem;
