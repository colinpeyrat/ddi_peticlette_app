var HistoricalListItem = require('./list-item');

var PathList = Backbone.View.extend({

    tagName: 'div',
    id: 'pathList',
    className: 'path-list path-list--with-date',

    user: null,

    initialize: function (options) {
        this.listenTo(this.model, 'sync', this.render);

        this.user = options.user || {};
    },

    addItem: function (model) {
        var item;
        item = new HistoricalListItem({
            model: model
        });
        this.$el.append(item.el);
    },

    render: function () {

        var pathsOfUser = this.model.getByUser(this.user.id);

        _.each(pathsOfUser, function (path) {
            this.addItem(path);
        }, this);

        // this.model.each(function (model) {
        //     console.log(model);
        //     this.addItem(model);
        // }, this);

        return this;
    }

});

module.exports = PathList;