var ModalDelete = require('./profile/modal-delete');


var ProfileView = Backbone.View.extend({

    tagName: 'div',
    className: 'content-inner',
    id: 'contentInner',

    template: _.template(require('../templates/profile.html')),

    events: {
        'click .children-list-item__remove': 'openDialog'
    },

    initialize: function () {
        console.log('view/path-view: initialize');

        this.modal = new ModalDelete();

        this.listenTo(this.modal, 'delete', this.deleteChildren);
        this.listenTo(this.model, 'sync', this.render);

        this.render();
    },

    enter: function () {
        console.log('view/path-view: enter');
        TweenMax.from(this.el, 0.25, {autoAlpha: 0, ease: Sine.easeOut, force3D: true});
    },

    openDialog: function (e) {
        e.preventDefault();
        var childrendId = $(e.currentTarget).data('children-id');
        this.modal.toDelete = childrendId;
        TweenMax.to(this.el, 0.2, {scrollTo: 0, ease: Sine.easeOut});
        TweenMax.delayedCall(0.05, function () {
            this.modal.show();
        }, {}, this);
    },

    deleteChildren: function (id) {
        this.model.deleteChildren(id);
    },

    leave: function (callback) {
        console.log('view/path-view: leave');
        TweenMax.to(this.el, 0.15, {
            autoAlpha: 0,
            ease: Sine.easeOut,
            force3D: true,
            onComplete: function () {
                callback();
            }
        });
    },

    render: function () {
        this.$el.html(this.template(this.model.toJSON()));
        this.modal.setElement(this.$('#modalWrapper')).render();
        return this;
    }
});

module.exports = ProfileView;