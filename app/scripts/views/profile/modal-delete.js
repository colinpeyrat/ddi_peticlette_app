var ModalBook = Backbone.View.extend({

    tagName: 'div',
    className: 'modal modal--delete-children',
    enabled: false,
    toDelete: null,

    template: _.template(require('../../templates/profile/modal-delete.html')),

    events: {
        'click #modalOverlay': 'onOverlayClick',
        'click #deleteBtn': 'onDelete',
        'click #cancelBtn': 'onCancel'
    },


    // Dans une classe backbone par convention initialize est la premiere méthode et render la derniere
    initialize: function (options) {

    },

    onOverlayClick: function () {
        this.hide();
    },

    onCancel: function (e) {
        e.preventDefault();
        this.hide();
    },

    onDelete: function (e) {
        e.preventDefault();
        this.trigger('delete', this.toDelete);
        this.hide();
    },

    show: function () {
        this.enabled = true;
        var $modal = this.$('#modalInner');
        var $modalOverlay = this.$('#modalOverlay');

        $('#contentInner').addClass('no-scroll');

        TweenMax.killTweensOf([$modal, $modalOverlay]);
        TweenMax.set([$modal, $modalOverlay], {autoAlpha: 1, force3D: true});
        TweenMax.fromTo($modalOverlay, 0.4, {
                scale: 0,
                force3D: true
            },
            {
                scale: 1,
                delay: 0.05,
                ease: Sine.easeOut,
                force3D: true
            });
        TweenMax.fromTo($modal, 0.3, {
                scale: 0,
                autoAlpha: 0,
                force3D: true
            },
            {
                scale: 1,
                autoAlpha: 1,
                ease: Sine.easeOut,
                force3D: true
            });
    },

    hide: function () {
        this.enabled = false;
        var $modal = this.$('#modalInner');
        var $modalOverlay = this.$('#modalOverlay');

        TweenMax.to($modal, 0.2, {
            scale: 0.7, autoAlpha: 0, ease: Sine.easeIn, force3D: true, onComplete: function () {
                $('#contentInner').removeClass('no-scroll');
            }
        });
        TweenMax.to($modalOverlay, 0.3, {scale: 0, ease: Sine.easeIn, delay: 0.05, force3D: true});
    },

    render: function () {
        this.$el.html(this.template());
        TweenMax.set([this.$('#modalInner'), this.$('#modalOverlay')], {autoAlpha: 0});

        return this;
    }

});

module.exports = ModalBook;
