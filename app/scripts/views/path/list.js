var PathListItem = require('./list-item');

var PathList = Backbone.View.extend({

    tagName: 'div',
    id: 'pathList',
    className: 'path-list',

    initialize: function () {
        this.listenTo(this.model, 'sync', this.render);
    },

    addItem: function (model) {
        var item;
        item = new PathListItem({
            model: model
        });
        this.$el.append(item.el);
    },

    render: function () {
        this.model.each(function (model) {
            this.addItem(model);
        }, this);

        return this;
    }

});

module.exports = PathList;