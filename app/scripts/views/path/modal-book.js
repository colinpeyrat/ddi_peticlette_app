var ModalBook = Backbone.View.extend({

    tagName: 'div',
    className: 'modal modal--book',
    enabled: false,
    userBooking: null,
    selected: [],

    $modalMessage: null,

    template: _.template(require('../../templates/path/modal-book.html')),
    templateRemainingBookingError: _.template('Désolé <%= firstname %>, il ne reste plus que <%= remainingBooking %> place(s)'),

    events: {
        'click #modalOverlay': 'onOverlayClick',
        'click .children-list-item__link': 'onChildrenListClick',
        'click #bookBtn': 'onSubmit'
    },


    // Dans une classe backbone par convention initialize est la premiere méthode et render la derniere
    initialize: function (options) {
        this.path = options.path || {};

        this.remainingBooking = options.remainingBooking;

        if (!_.isEmpty(this.path)) {
            this.userBooking = this.path.getBooking(this.model.get('_id'));
        }
    },

    onChildrenListClick: function (e) {
        e.preventDefault();
        e.stopPropagation();

        var item = $(e.currentTarget);

        var id = item.data('children-id');
        item.toggleClass('is-selected');

        if (!_.contains(this.selected, id)) {
            this.selected.push(id);
        } else {
            this.selected = _.without(this.selected, id);
        }
    },

    onOverlayClick: function () {
        this.hide();
    },

    onSubmit: function (e) {
        e.preventDefault();

        var remainingBookingWithoutUser = this.remainingBooking;

        // we remove a place only if it's a booking not by the current user
        _.each(this.path.get('bookings'), function (booking) {
            if (booking.parent !== this.model.id) {
                remainingBookingWithoutUser -= booking.childrens.length;
            }
        }.bind(this));


        if (this.selected.length > this.remainingBookingWithoutUser) {

            this.$modalMessage.html(this.templateRemainingBookingError({
                firstname: this.model.get('firstname'),
                remainingBookingWithoutUser: this.remainingBookingWithoutUser
            }));

        } else {
            this.$modalMessage.empty();

            var booking = {
                _id: _.uniqueId(),
                parent: this.model.get('_id'),
                childrens: this.selected
            };

            this.path.addBooking(this.model.get('_id'), booking);

            this.trigger('submit');

            TweenMax.delayedCall(0.1, function () {
                this.hide();
            }.bind(this));
        }

    },

    show: function () {
        this.enabled = true;
        var $modal = this.$('#modalInner');
        var $modalOverlay = this.$('#modalOverlay');

        $('#contentInner').addClass('no-scroll');

        TweenMax.killTweensOf([$modal, $modalOverlay]);
        TweenMax.set([$modal, $modalOverlay], {autoAlpha: 1, force3D: true});
        TweenMax.fromTo($modalOverlay, 0.4, {
                scale: 0,
                force3D: true
            },
            {
                scale: 1,
                delay: 0.05,
                ease: Sine.easeOut,
                force3D: true
            });
        TweenMax.fromTo($modal, 0.3, {
                scale: 0,
                autoAlpha: 0,
                force3D: true
            },
            {
                scale: 1,
                autoAlpha: 1,
                ease: Sine.easeOut,
                force3D: true
            });
    },

    hide: function () {
        this.enabled = false;
        var $modal = this.$('#modalInner');
        var $modalOverlay = this.$('#modalOverlay');

        TweenMax.to($modal, 0.2, {
            scale: 0.7, autoAlpha: 0, ease: Sine.easeIn, force3D: true, onComplete: function () {
                $('#contentInner').removeClass('no-scroll');
            }
        });
        TweenMax.to($modalOverlay, 0.3, {scale: 0, ease: Sine.easeIn, delay: 0.05, force3D: true});
    },

    render: function () {
        this.$el.html(this.template(this.model.toJSON()));
        TweenMax.set([this.$('#modalInner'), this.$('#modalOverlay')], {autoAlpha: 0});

        // if user has already book this path we select the booked children
        if (this.userBooking) {
            this.selected = this.userBooking.childrens;

            _.each(this.userBooking.childrens, function (children) {
                this.$('.children-list-item__link[data-children-id="' + children + '"]').addClass('is-selected')
            }.bind(this));
        }

        this.$modalMessage = this.$('#modalMessage');

        return this;
    }

});

module.exports = ModalBook;
