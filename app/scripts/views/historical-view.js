var HistoricalListView = require('./historical/list');

var EventBus = require('../helpers/event-bus');

var HistoricalView = Backbone.View.extend({

    tagName: 'div',
    className: 'content-inner',
    id: 'contentInner',

    template: _.template(require('../templates/historical.html')),

    events: {
        'click #goToPathBtn': 'onClickPathBtn'
    },

    initialize: function (options) {
        console.log('view/historical-view: initialize');

        var user = options.user || {};

        this.model.fetch();

        this.list = new HistoricalListView({
            model: this.model,
            user: user
        });

        this.render();
    },

    onClickPathBtn: function () {
        EventBus.trigger('menu:change', 'path');
    },

    enter: function () {
        console.log('view/historical-view: enter');
        TweenMax.set(this.$('#pathListWrapper'), {autoAlpha: 1});
        TweenMax.from(this.el, 0.25, {autoAlpha: 0, ease: Sine.easeOut, force3D: true});
        // TweenMax.to(this.$("#pathListWrapper"), 0.15, {autoAlpha: 1, force3D: true});
    },

    leave: function (callback) {
        console.log('view/historical-view: leave');
        TweenMax.set(this.$('#pathListWrapper'), {autoAlpha: 0});
        TweenMax.to(this.el, 0.15, {
            autoAlpha: 0,
            ease: Sine.easeOut,
            force3D: true,
            onComplete: function () {
                callback();
            }
        });
    },

    render: function () {
        this.$el.html(this.template());
        this.$('#pathListWrapper').append(this.list.$el);
        return this;
    }
});

module.exports = HistoricalView;