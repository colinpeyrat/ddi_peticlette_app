var PathListView = require('./path/list');

var PathView = Backbone.View.extend({

    tagName: 'div',
    className: 'content-inner',
    id: 'contentInner',

    template: _.template(require('../templates/path.html')),

    events: {},

    list: null,

    initialize: function () {
        console.log('view/path-view: initialize');

        this.model.fetch();

        this.list = new PathListView({model: this.model});

        this.render();
    },

    enter: function () {
        console.log('view/path-view: enter');
        TweenMax.from(this.el, 0.25, {autoAlpha: 0, ease: Sine.easeOut, force3D: true});
    },

    leave: function (callback) {
        console.log('view/path-view: leave');
        TweenMax.to(this.el, 0.15, {
            autoAlpha: 0,
            ease: Sine.easeOut,
            force3D: true,
            onComplete: function () {
                callback();
            }
        });
    },

    render: function () {
        this.$el.html(this.template());
        this.$('#pathListWrapper').append(this.list.$el);
        return this;
    }
});

module.exports = PathView;