var EventBus = require('../helpers/event-bus');

var menuView = Backbone.View.extend({

    el: '#menu',
    template: _.template(require('../templates/menu.html')),
    enabled: true,

    events: {
        'click .menu-link': 'toggleMenuItem'
    },

    initialize: function () {
        console.log('view/menu-view: initialize');

        this.listenTo(EventBus, 'menu:change', this.changeActive);

        this.render();
    },

    render: function () {
        if (this.enabled) {
            this.$el.html(this.template());
        } else if (!this.enabled) {
            this.$el.empty();
        }
    },

    toggleMenuItem: function (e) {
        e.stopPropagation();
        this.$('.menu-link').removeClass('is-active');
        $(e.currentTarget).toggleClass('is-active');
    },

    changeActive: function (item) {
        this.$('.menu-link').removeClass('is-active');
        this.$('.menu-link[href="#' + item + '"]').addClass('is-active');
    },

    enable: function () {
        this.$el.removeClass('is-disabled');
        this.enabled = true;
    },

    disable: function () {
        this.$el.addClass('is-disabled');
        this.enabled = false;
    }
});

module.exports = menuView;