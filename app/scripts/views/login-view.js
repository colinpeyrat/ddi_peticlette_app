var EventBus = require('../helpers/event-bus');

var LoginView = Backbone.View.extend({

    tagName: 'div',
    className: 'content-inner',
    id: 'contentInner',

    hasError: false,
    isLogged: false,

    $login: null,
    $password: null,
    $messageWrapper: null,

    template: _.template(require('../templates/login.html')),

    events: {
        'click #loginBtn': 'login'
    },

    initialize: function () {
        console.log('view/login-view: initialize');

        this.render();
    },

    enter: function () {
        console.log('view/login-view: enter');
        TweenMax.from(this.el, 0.25, {autoAlpha: 0, x: 200, ease: Sine.easeOut, force3D: true});
    },

    leave: function (callback) {
        console.log('view/login-view: leave');
        TweenMax.to(this.el, 0.15, {
            autoAlpha: 0,
            x: 200,
            ease: Sine.easeOut,
            force3D: true,
            onComplete: function () {
                callback();
            }
        });
    },

    login: function (e) {
        e.preventDefault();
        e.stopPropagation();

        _.each([this.$login, this.$password], function (value) {
            if (!value.val().length) {
                value.addClass('has-error');
                this.hasError = true;
            } else {
                value.removeClass('has-error');
                this.hasError = false;
            }
        }, this);


        var login = this.$login.val();
        var password = this.$password.val();

        this.model.each(function (user) {
            if (user.get('login') === login && user.get('password') === password) {
                this.isLogged = true;
                EventBus.trigger('user:logged', user);
            }
        }.bind(this));


        if (!this.hasError) {
            if (this.isLogged) {
                EventBus.trigger('menu:change', 'path');
                Backbone.history.navigate('path', true);
            } else {
                this.$messageWrapper.html('Les informations que vous avez saisies sont incorrectes, Veuillez vérifier et ressayez')
            }
        }
    },

    render: function () {
        this.$el.html(this.template());

        this.$login = this.$('#login');
        this.$password = this.$('#password');
        this.$messageWrapper = this.$('#messageWrapper');

        return this;
    }
});

module.exports = LoginView;