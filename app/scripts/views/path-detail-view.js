var ModalBook = require('./path/modal-book');

var EventBus = require('../helpers/event-bus');

var PathDetailView = Backbone.View.extend({

    tagName: 'div',
    className: 'content-inner',
    id: 'contentInner',

    list: null,
    pathMap: null,

    template: _.template(require('../templates/path-detail.html')),

    events: {
        'click #pathBookBtn': 'onBookButtonClick'
    },

    initialize: function (options) {
        console.log('view/path-detail-view: initialize');

        // Verify if the view has a model
        if (!this.model) {
            EventBus.trigger('menu:change', 'path');
            Backbone.history.navigate('path', true);
        }

        this.user = options.user || {};

        this.modal = new ModalBook({
            model: this.user,
            path: this.model,
            remainingBooking: this.getRemainingBooking()
        });

        this.listenTo(this.modal, 'submit', this.updateRemainingBooking);

        this.render();
    },

    updateRemainingBooking: function () {
        console.log('view/path-view: updateRemainingBooking');
        var remainingBooking = this.getRemainingBooking();
        this.modal.remainingBooking = remainingBooking;
        this.$('#remainingBooking').text(remainingBooking);
    },

    onBookButtonClick: function (e) {
        e.preventDefault();

        // Go to top of the window where the modal is open
        TweenMax.to(this.el, 0.15, {scrollTo: 0, ease: Sine.easeOut});
        TweenMax.delayedCall(0.125, function () {
            this.modal.show();
        }, {}, this);
    },

    getRemainingBooking: function () {
        var remainingBooking = this.model.get('bookingsLimit');

        _.each(this.model.get('bookings'), function (booking) {
            remainingBooking -= booking.childrens.length;
        }.bind(this));

        return remainingBooking;
    },

    calculateAndDisplayRoute: function (directionsService, directionsDisplay, startPoint, endPoint) {
        directionsService.route({
            origin: startPoint,
            destination: endPoint,
            travelMode: google.maps.TravelMode.BICYCLING
        }, function (response, status) {
            if (status == google.maps.DirectionsStatus.OK) {
                directionsDisplay.setDirections(response);
            } else {
                window.alert('Directions request failed due to ' + status);
            }
        });
    },


    initMap: function () {

        this.stylesMap = [
            {
                'featureType': 'administrative',
                'elementType': 'labels',
                'stylers': [
                    {
                        'visibility': 'simplified'
                    }
                ]
            },
            {
                'featureType': 'administrative',
                'elementType': 'labels.text.fill',
                'stylers': [
                    {
                        'color': '#f95c7a'
                    }
                ]
            },
            {
                'featureType': 'landscape',
                'elementType': 'all',
                'stylers': [
                    {
                        'color': '#f4f3ef'
                    }
                ]
            },
            {
                'featureType': 'poi',
                'elementType': 'all',
                'stylers': [
                    {
                        'visibility': 'simplified'
                    }
                ]
            },
            {
                'featureType': 'poi',
                'elementType': 'labels',
                'stylers': [
                    {
                        'visibility': 'simplified'
                    }
                ]
            },
            {
                'featureType': 'road',
                'elementType': 'all',
                'stylers': [
                    {
                        'saturation': -100
                    },
                    {
                        'lightness': 45
                    }
                ]
            },
            {
                'featureType': 'road',
                'elementType': 'labels',
                'stylers': [
                    {
                        'visibility': 'simplified'
                    },
                    {
                        'hue': '#ff0000'
                    }
                ]
            },
            {
                'featureType': 'road',
                'elementType': 'labels.text.fill',
                'stylers': [
                    {
                        'color': '#293544'
                    }
                ]
            },
            {
                'featureType': 'road.highway',
                'elementType': 'all',
                'stylers': [
                    {
                        'visibility': 'simplified'
                    }
                ]
            },
            {
                'featureType': 'road.arterial',
                'elementType': 'labels.icon',
                'stylers': [
                    {
                        'visibility': 'off'
                    }
                ]
            },
            {
                'featureType': 'transit',
                'elementType': 'all',
                'stylers': [
                    {
                        'visibility': 'off'
                    }
                ]
            },
            {
                'featureType': 'water',
                'elementType': 'all',
                'stylers': [
                    {
                        'color': '#293544'
                    },
                    {
                        'visibility': 'on'
                    }
                ]
            }
        ];

        var pathEnd = this.model.get('end');
        var pathStart = this.model.get('start');

        this.pathMap = new google.maps.Map(this.el.querySelector('#pathMap'), {
            center: new google.maps.LatLng(pathEnd.pos.lat, pathEnd.pos.lng),
            zoom: 15,
            disableDefaultUI: true
        });

        this.directionsService = new google.maps.DirectionsService;
        this.directionsDisplay = new google.maps.DirectionsRenderer({
            map: this.pathMap
        });

        this.calculateAndDisplayRoute(this.directionsService, this.directionsDisplay, {
            lat: pathStart.pos.lat,
            lng: pathStart.pos.lng
        }, {
            lat: pathEnd.pos.lat,
            lng: pathEnd.pos.lng
        });
        this.pathMap.setOptions({styles: this.stylesMap})
    },

    enter: function () {
        console.log('view/path-view: enter');
        TweenMax.from(this.el, 0.25, {autoAlpha: 0, y: 200, ease: Sine.easeOut, force3D: true});
        google.maps.event.trigger(this.pathMap, 'resize');
    },

    leave: function (callback) {
        console.log('view/path-view: leave');
        TweenMax.to(this.el, 0.15, {
            autoAlpha: 0,
            y: 200,
            ease: Sine.easeOut,
            force3D: true,
            onComplete: function () {
                this.modal.hide();
                callback();
            }.bind(this)
        });
    },

    render: function () {

        this.$el.html(this.template({
                path: this.model.toJSON(),
                remainingBooking: this.getRemainingBooking()
            })
        );

        this.initMap();

        // I used `setElement` and not `append` to delegate events in the subview
        this.modal.setElement(this.$('#modalWrapper')).render();

        return this;

    }
});

module.exports = PathDetailView;