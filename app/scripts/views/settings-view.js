var EventBus = require('../helpers/event-bus');

var SettingsView = Backbone.View.extend({

    tagName: 'div',
    className: 'content-inner',
    id: 'contentInner',

    template: _.template(require('../templates/settings.html')),

    events: {
        'click #logout': 'logout'
    },

    initialize: function () {
        console.log('view/settings-view: initialize');

        this.render();
    },

    logout: function (e) {
        e.preventDefault();
        e.stopPropagation();

        EventBus.trigger('user:logout');
    },

    enter: function () {
        console.log('view/settings-view: enter');
        TweenMax.from(this.el, 0.25, {autoAlpha: 0, ease: Sine.easeOut, force3D: true});
    },

    leave: function (callback) {
        console.log('view/settings-view: leave');
        TweenMax.to(this.el, 0.15, {
            autoAlpha: 0,
            ease: Sine.easeOut,
            force3D: true,
            onComplete: function () {
                callback();
            }
        });
    },

    render: function () {
        this.$el.html(this.template());
        return this;
    }
});

module.exports = SettingsView;